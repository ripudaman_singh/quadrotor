// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz
#define thresh_X 0.10
#define thresh_Y 0.10
#define thresh_ALT 0.03
#define thresh_YAW 0.10
#define halfClosed 80
#define closedAngle 56
#define openAngle 170

#include "../include/quadcopter_main.h"

double x[1000] = {0};
double y[1000] = {0};
double alt[1000] = {0};
double yaw[1000] = {0};
int file_Length = 0;
int stateNum = 0;
int next_state = 0;
int gripperflag = 0; //slightly closed. default for take off //0 default, 1 open, 2 closed, 3 unknown
int perch_success = 0; //0 never done, 1 success, 2 failed
int reached_count = 0;
int wp_hold = 0;
int first_wp = 0;
int test_gripper = 1;
int autocontrol = 0; 

void read_config(char *);
int processing_loop_initialize();
/*
 * State estimation and guidance thread
 */

void myflush ( FILE *in )
{
  int ch;

  do
    ch = fgetc ( in ); 
  while ( ch != EOF && ch != '\n' ); 

  clearerr ( in );
}

void mypause ( void ) 
{ 
  printf ( "Press Enter to continue . . ." );
  fflush ( stdout );
  getchar();
} 

int test_Status(DynamStatus *status1 , DynamStatus *status2 ,double angle)
{
    if ((abs(status2->cur_angle - angle) < 1) && (abs(status2->cur_angle - angle) < 1)){
        printf("Success\n");
        return 1;
    }
    else{
        printf("Failure\n");
        return 0;
    }
}
int test_Speed(DynamStatus *status1 , DynamStatus *status2)
{
    if ((status1->cur_speed == 0 ) && (status2->cur_speed == 0)){
        return 1;
    }
    else return 0;
}

void *processing_loop(void *data)
{
    int hz = PROC_FREQ;

    processing_loop_initialize();
    // Local copies
    struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
    struct state localstate;
    int first_time = 1;
    int set_point_num = 0;
    int16_t new_state[4] = {0};
    int file_direction = 1;

    while (1) 
    {  // Main thread loop
 
        pthread_mutex_lock(&mcap_mutex);
        mcobs[0] = mcap_obs;
        if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
        else               mcobs[1] = mcap_obs;
        pthread_mutex_unlock(&mcap_mutex);
        
        pthread_mutex_lock(&state_mutex);
        memcpy(&localstate, state, sizeof(struct state));
        pthread_mutex_unlock(&state_mutex);

        localstate.time = mcobs[0]->time;
        localstate.pose[0] = mcobs[0]->pose[0]; // x position
        localstate.pose[1] = mcobs[0]->pose[1]; // y position
        localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
        localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
        localstate.pose[4] = 0;
        localstate.pose[5] = 0;
        localstate.pose[6] = 0;
        localstate.pose[7] = 0;
        
        // Estimate velocities from first-order differentiation
        double mc_time_step = mcobs[0]->time - mcobs[1]->time;
        if(mc_time_step > 1.0E-7 && mc_time_step < 1)
        {
            localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
            localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
            localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
            localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
            localstate.data = 1;
        }
        else
        {
            // Change flag is data drop off occurs
            localstate.data = 0;
            printf("DATA DROPOFF\n");
        }

        // For the first run just set the first set points
        if (first_time)
        {
            printf("UPDATED\n");
            localstate.set_points[0] = x[set_point_num];
            localstate.set_points[1] = y[set_point_num];
            localstate.set_points[2] = alt[set_point_num];
            localstate.set_points[3] = yaw[set_point_num];
            localstate.set_points[4] = 0;
            localstate.set_points[5] = 0;
            localstate.set_points[6] = 0;
            first_time = 0;
            stateNum = 6;
            next_state = 0;
            printf( "%lf %lf %lf %lf\n", localstate.set_points[0],localstate.set_points[1],localstate.set_points[2],localstate.set_points[3]);
        }
       // printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);

        // FSM
        // 0 - Waypoint follow
        // 1 - Gripper opening
        // 2 - Reached Final waypoint, Gripper close
        // 3 -
        // 4 -
        // 5 - 
        // 6 - Hold State
        // sleep(0.5);
        //printf( "%lf %lf %lf %lf\n", localstate.pose[0],localstate.pose[1],localstate.pose[2],localstate.pose[3]);
        if(localstate.fence_on == 1)
        {
            
            if(wp_hold && first_wp)
            {
                printf("--------------------------------------------SETPOINT IS SET--------------------------------------------------\n");
                localstate.set_points[0] = localstate.pose[0];
                localstate.set_points[1] = localstate.pose[1];
                localstate.set_points[2] = localstate.pose[2];
                localstate.set_points[3] = localstate.pose[3];
                // printf("before\n");
                printf( "%lf %lf %lf %lf\n", localstate.set_points[0],localstate.set_points[1],localstate.set_points[2],localstate.set_points[3]);
                // printf("after\n");
                first_wp = 0;
            }
            // printf( "%lf %lf %lf %lf\n", localstate.pose[0],localstate.pose[1],localstate.pose[2],localstate.pose[3]);
            // printf( "%lf %lf %lf %lf\n", localstate.set_points[0],localstate.set_points[1],localstate.set_points[2],localstate.set_points[3]);
            // auto_control((float *) localstate.pose, localstate.set_points,new_state);
            switch(stateNum) 
            {
                case 0:// just follow waypoints
                    // printf("%s %d %d %d %d\n", "PWM VALUES", new_state[0], new_state[1] ,new_state[2] ,new_state[3]);
                    // printf("State 0\n");
                    if (update_set_points(localstate.pose, localstate.set_points, first_time))//Checks if within threshold
                    {
                        // first_time = 0;
                        if (set_point_num < file_Length)
                        {
                            // printf("FORWARD\n");   
                               set_point_num++;
                            if (set_point_num == file_Length && file_direction == 1)
                            {   
                                set_point_num--;
                                file_direction = -1;
                            }
                            localstate.set_pt_no = set_point_num;
                        }

                        else if (set_point_num >= 0 && file_direction == -1)
                        {
                            // printf("Reverse\n");
                            set_point_num--;
                            if (set_point_num < 0)
                                set_point_num = 0;
                            localstate.set_pt_no = set_point_num;
                            
                        }
                        
                        //update set_points
                        // If position hold, then set the same points again
                        if (wp_hold)
                        {
                            localstate.set_points[0] = localstate.set_points[0];
                            localstate.set_points[1] = localstate.set_points[1];
                            localstate.set_points[2] = localstate.set_points[2];
                            localstate.set_points[3] = localstate.set_points[3];
                            localstate.set_points[4] = 0;
                            localstate.set_points[5] = 0;
                            localstate.set_points[6] = 0;
                        }
                        // If not position hold, then update to the next point
                        else
                        {
                            localstate.set_points[0] = x[set_point_num];
                            localstate.set_points[1] = y[set_point_num];
                            localstate.set_points[2] = alt[set_point_num];
                            localstate.set_points[3] = yaw[set_point_num];
                            localstate.set_points[4] = 0;
                            localstate.set_points[5] = 0;
                            localstate.set_points[6] = 0;
                        }
                        // localstate.set_points[7] = 0;

                        //printf("%s\n","WAYPOINT REACHED AND UPDATED" );

                        //printf("%s %d\n", "PRESENT SETPOINT = ",set_point_num);

                        if (set_point_num%2 == 0)
                        {
                            stateNum = 6;
                            next_state = 0;
                        }
                        
                        // If it is the last setpoint
                        if (test_gripper)
                        {
                            if (set_point_num == file_Length-1)
                            {
                            printf("Switch to Wheel Mode \n");
                            pthread_mutex_lock(&dynamixel_mutex);
                            bus.servo[0].cmd_flag = CMD;
                            bus.servo[0].cmd_mode = WHEEL;
                            bus.servo[0].cmd_flag = MODE;
                            bus.servo[1].cmd_flag = CMD;
                            bus.servo[1].cmd_mode = WHEEL;
                            bus.servo[1].cmd_flag = MODE;                   
                            pthread_mutex_unlock(&dynamixel_mutex);
                            gripperflag = 0;
                            stateNum = 6;
                            next_state = 2;
                            }  
                            
                            else if(set_point_num == 2 && gripperflag != 1)
                            {
                            printf("Switch to Joint Mode \n");
                            stateNum = 1;// next set point is intermediate waypoint
                            pthread_mutex_lock(&dynamixel_mutex);
                            bus.servo[0].cmd_flag = CMD;
                            bus.servo[0].cmd_mode = JOINT; 
                            bus.servo[0].cmd_flag = MODE;
                            bus.servo[1].cmd_flag = CMD;
                            bus.servo[1].cmd_mode = JOINT; 
                            bus.servo[1].cmd_flag = MODE;
                            pthread_mutex_unlock(&dynamixel_mutex);
                            gripperflag = 0;   
                            }
                            else
                            {
                            stateNum = 6;
                            next_state = 0;
                            }
                        }
                    }
                    break;
                case 1: // intermediate waypoint //open wide
                    // go and hover at the intermediate set point
                    // gripper open
                    if (gripperflag == 0)
                    {
                        printf("Move to Open\n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_angle = openAngle; 
                        bus.servo[0].cmd_speed = 0.7; 
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[1].cmd_angle = openAngle; 
                        bus.servo[1].cmd_speed = 0.7; 
                        bus.servo[1].cmd_flag = CMD;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        gripperflag = 3;
                    }
                    else if (gripperflag == 3)
                    {
                        printf("Check Status \n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = STATUS;
                        bus.servo[1].cmd_flag = STATUS;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        sleep(2);
                    }

                    if(test_Status(&(bus.servo[0]),&(bus.servo[1]),openAngle))// check if open completely
                    {
                        if(perch_success > 0) // after a grip
                        {
                            stateNum = 4;
                        }
                        else
                        { // before attempting perch operation
                            stateNum = 0;
                            gripperflag = 1;
                        }
                    }
                    break;
                case 2: // final waypoint // About to close
                    if (gripperflag == 3)
                    {
                        printf("Check Status\n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = STATUS;
                        bus.servo[1].cmd_flag = STATUS;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        sleep(2);
                    }
                    if (gripperflag == 0)
                    {
                        printf("Wheel Close\n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_torque = 1; 
                        bus.servo[0].cmd_speed = -1; 
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[1].cmd_torque = 1; 
                        bus.servo[1].cmd_speed = -1; 
                        bus.servo[1].cmd_flag = CMD;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        gripperflag = 3;
                    }
                    else if (test_Speed(&(bus.servo[0]),&(bus.servo[1]))){
                        if(test_Status(&(bus.servo[0]),&(bus.servo[1]),closedAngle))// check if successfully closed
                        {
                            stateNum = 2;
                            perch_success = 1;
                            // turn off quad
                            mypause();
                            gripperflag = 2;
                        }
                        else 
                        {
                            printf("Switch to Joint Mode \n");
                            stateNum=1; // open gripper wide
                            perch_success = 2;
                            gripperflag = 0;   
                            pthread_mutex_lock(&dynamixel_mutex);
                            bus.servo[0].cmd_flag = CMD;
                            bus.servo[0].cmd_mode = JOINT; 
                            bus.servo[0].cmd_flag = MODE;
                            bus.servo[1].cmd_flag = CMD;
                            bus.servo[1].cmd_mode = JOINT; 
                            bus.servo[1].cmd_flag = MODE;
                            pthread_mutex_unlock(&dynamixel_mutex);
                        }
                    }
                    break;
                case 3:
                    // wait for command to take off again
                    if (true)
                    {
                        printf("Switch to Joint Mode \n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[0].cmd_mode = JOINT; 
                        bus.servo[0].cmd_flag = MODE;
                        bus.servo[1].cmd_flag = CMD;
                        bus.servo[1].cmd_mode = JOINT; 
                        bus.servo[1].cmd_flag = MODE;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        gripperflag = 0;   
                        stateNum = 1;
                    }
                    break;
                case 4:
                    printf("%s %d %d %d %d\n", "PWM VALUES", new_state[0], new_state[1] ,new_state[2] ,new_state[3]);
                    if (update_set_points(localstate.pose, localstate.set_points, first_time))// Checks if within threshold
                    {
                        
                        printf("%s\n","WAYPOINT REACHED" );
                        // update set_points
                        localstate.set_points[0] = x[set_point_num];
                        localstate.set_points[1] = y[set_point_num];
                        localstate.set_points[2] = alt[set_point_num];
                        localstate.set_points[3] = yaw[set_point_num];
                        localstate.set_points[4] = 0;
                        localstate.set_points[5] = 0;
                        localstate.set_points[6] = 0;
                        localstate.set_points[7] = 0;
                        
                        if (set_point_num > 0)
                        {
                            set_point_num--;
                            localstate.set_pt_no = set_point_num;
                        }
                        if(set_point_num == 0 && gripperflag == 0)
                        {
                            printf("%s\n", "Done take off");
                            stateNum=5;
                        }
                        
                        else if(set_point_num == file_Length/2 && perch_success==2)
                        {
                            stateNum=0;// try to land again
                            
                        }
                        else if(set_point_num == file_Length/2 && perch_success==1)
                        {
                            printf("Close Grip \n");
                            pthread_mutex_lock(&dynamixel_mutex);
                            bus.servo[0].cmd_angle = halfClosed;
                            bus.servo[0].cmd_speed = 0.4;
                            bus.servo[0].cmd_flag = CMD;
                            bus.servo[1].cmd_angle = halfClosed;
                            bus.servo[1].cmd_speed = 0.4;
                            bus.servo[1].cmd_flag = CMD;
                            pthread_mutex_unlock(&dynamixel_mutex);  
                            gripperflag = 0;
                        }
                    }
                    break;
                case 5:
                    localstate.fence_on = 0;
                    break;
                case 6: // Hold Point
                    // printf("State 6\n");
                    // If we have reached a setpoint
                    if (update_set_points(localstate.pose, localstate.set_points, first_time) && ((set_point_num%2) == 0))
                    {
                        // Increment reached counter
                        printf("--------------------------------------------SETPOINT INCREMENT--------------------------------------------------\n");
                        reached_count++;
                    }
                    else
                    {
                        // If it is a waypoint then just move on to state 0
                        if ((set_point_num%2) == 1)
                        {
                            reached_count = 100;
                        }
                        else
                        {
                            // Reset the count
                            reached_count = 0;
                        }    
                    }

                    // If its a setpoint and we have not stayed there for atleast 10 iterations
                    if (reached_count < 20)
                    {
                        // Stay here
                        stateNum = 6;
                    }
                    else
                    {
                        // Go to the next waypoint state
                        stateNum = next_state;
                    }

                    // localstate.set_points[0] = x[set_point_num];
                    // localstate.set_points[1] = y[set_point_num];
                    // localstate.set_points[2] = alt[set_point_num];
                    // localstate.set_points[3] = yaw[set_point_num];
                    // localstate.set_points[4] = 0;
                    // localstate.set_points[5] = 0;
                    // localstate.set_points[6] = 0;
                    break;
            }
        }
        else
        {
            first_wp = 1;
        }
        // Copy to global state (minimize total time state is locked by the mutex)
        pthread_mutex_lock(&state_mutex);
        memcpy(state, &localstate, sizeof(struct state));
        pthread_mutex_unlock(&state_mutex);
        
        usleep(1000000/hz);
        
    } // end while processing_loop()
    
    return 0;
}

/**
 * update_set_points()
 */
int update_set_points(double* pose, float *set_points, int first_time)
{
    if (first_time) 
      return 1;

    int valid = 0;
    // float temp = pose[0] - set_points[0];
    // printf("%f\n",temp);
    if (fabs(pose[0] - set_points[0]) > thresh_X)
      valid ++;
    if (fabs(pose[1] - set_points[1]) > thresh_Y)
      valid ++;
    if (fabs(pose[2] - set_points[2]) > thresh_ALT)
      valid ++;
    if (fabs(pose[3] - set_points[3]) > thresh_YAW)
      valid++;

    // printf("%d\n", valid);
    if (valid > 0)
      return 0;
    else
      return 1;
}


/**
 * processing_loop_initialize()
 */
int processing_loop_initialize()
{
    // Initialize state struct
    state = (state_t*) calloc(1,sizeof(*state));
    state->time = ((double)utime_now())/1000000;
    memset(state->pose,0,sizeof(state->pose));
    
    // Read configuration file
    char blah[] = "config.txt";
    read_config(blah);
    
    mcap_obs[0].time = state->time;
    mcap_obs[1].time = -1.0;  // Signal that this isn't set yet
    
    // Initialize Altitude Velocity Data Structures
    memset(diff_z, 0, sizeof(diff_z));
    memset(diff_z_med, 0, sizeof(diff_z_med));
    
    // Fence variables
    state->fence_on = 0;
    memset(state->set_points,0,sizeof(state->set_points));
    state->time_fence_init = 0;
    
    /*// Initialize IMU data
    if(imu_mode == 'u' || imu_mode == 'r')
    {
        init_imu();
    }
    */
    return 0;
}

void read_config(char* config)
{
    // open configuration file
    FILE* conf = fopen(config,"r");
    // holder string
    char str[1000];
    double start[2];
    printf("Read\n");
    // read in the quadrotor initial position
    fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
    fscanf(conf,"%lf %lf %lf %lf",&x[0],&y[0],&alt[0],&yaw[0]);
    file_Length = 1;

    if(wp_hold)
    {
        while(fscanf(conf,"%lf %lf %lf %lf",&x[file_Length+1],&y[file_Length+1],&alt[file_Length+1],&yaw[file_Length+1]) != EOF)
        {
            x[file_Length] = (x[file_Length-1] + x[file_Length+1])/2;
            y[file_Length] = (y[file_Length-1] + y[file_Length+1])/2;
            alt[file_Length] = (alt[file_Length-1] + alt[file_Length+1])/2;
            yaw[file_Length] = (yaw[file_Length-1] + yaw[file_Length+1])/2;
            file_Length = file_Length + 2;
        }
        int i = 0;
        for (i =0; i < file_Length; ++i)
        {
            printf("%lf %lf %lf %lf\n",x[i],y[i],alt[i],yaw[i] );
        }

    }
    fclose(conf);
}
