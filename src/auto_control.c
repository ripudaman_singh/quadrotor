//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"

// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1575 // Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1455 // Lower saturation PWM limit. 

// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

#define p_X 1
#define p_Z 2
#define p_ALT 3
#define p_YAW 0

#define d_X 0
#define d_Z 0
#define d_ALT 0
#define d_YAW 0

// Outer loop controller to generate PWM signals for the Naza-M autopilot
// P-control
// Output - Proportional output for given channel
double p_controller(double pose_point, double set_point, double p_gain)
{
    double out = p_gain * (set_point - pose_point);
    return out;
}
// D - control
// Output - Differetial control for given channel
double d_controller(double pose_velocity, double d_gain)
{
    double out = d_gain * pose_velocity;
    return out;
}

int MAX(int a,int b)
{
  return a>b?a:b;
}

int MIN(int a,int b)
{
  return a<b?a:b;
}

// void auto_control(float *pose, float *set_points, int16_t* channels_ptr)
void auto_control(float *pose, float *set_points, int16_t* channels_ptr)
{  
  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!) 
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw
  // Changed Pitch and Roll channels, Chenge them back to original as mentioned above if issues occur
  int temp = 0;

  // P controller
  double px = p_controller(pose[0],set_points[0],p_X);
  double pz = p_controller(pose[1],set_points[1],p_Z);
  double palt = p_controller(pose[2],set_points[2],p_ALT);
  double pyaw = p_controller(pose[3],set_points[3],p_YAW);

  // D contoller
  double dx = d_controller(pose[4],d_X);
  double dz = d_controller(pose[5],d_Z);
  double dalt = d_controller(pose[6],d_ALT);
  double dyaw = d_controller(pose[7],d_YAW);

  temp = (int)((px * (pitch_PWM_forward - pitch_PWM_backward)) + pitch_PWM_base);
  temp = temp + dx;
  temp = MAX(temp,pitch_PWM_backward);
  channels_ptr[1] = (int16_t)MIN(temp,pitch_PWM_forward); // pitch

  temp = (int)((-pz * (roll_PWM_left - roll_PWM_right)) + roll_PWM_base);
  temp = temp + dz;
  temp = MAX(temp,roll_PWM_right);
  channels_ptr[2] = (int16_t)MIN(temp,roll_PWM_left); // roll

  temp = (int)((palt * (thrust_PWM_up - thrust_PWM_down)) + thrust_PWM_base);
  temp = temp + dalt;
  temp = MAX(temp,thrust_PWM_down);
  channels_ptr[0] = (int16_t)MIN(temp,thrust_PWM_up); // thrust

  temp = (int)((pyaw * (yaw_PWM_ccw - yaw_PWM_cw)) + yaw_PWM_base);
  temp = temp + dyaw;
  temp = MAX(temp,yaw_PWM_cw);
  channels_ptr[3] = (int16_t)MIN(temp,yaw_PWM_ccw); // yaw

  channels_ptr[4] = 0;
  channels_ptr[5] = 0;
  channels_ptr[6] = 0;
  channels_ptr[7] = 0;

  return;
}
