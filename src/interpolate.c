
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>


double x[1000] = {0};
double y[1000] = {0};
double alt[1000] = {0};
double yaw[1000] = {0};


int main(){
    int file_Length = 0;
    // open configuration file
    FILE* conf = fopen("config.txt","r");
    FILE* out = fopen("yo.txt","w");
    // holder string
    char str[1000];
    double start[2];

    // read in the quadrotor initial position
    fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
    fscanf(conf,"%lf %lf %lf %lf",&x[0],&y[0],&alt[0],&yaw[0]);
    file_Length = 1;
    while(fscanf(conf,"%lf %lf %lf %lf",&x[file_Length+1],&y[file_Length+1],&alt[file_Length+1],&yaw[file_Length+1]) != EOF){
        x[file_Length] = (x[file_Length-1] + x[file_Length+1])/2;
        y[file_Length] = (y[file_Length-1] + y[file_Length+1])/2;
        alt[file_Length] = (alt[file_Length-1] + alt[file_Length+1])/2;
        yaw[file_Length] = (yaw[file_Length-1] + yaw[file_Length+1])/2;
        file_Length = file_Length + 2;
    }

    int i = 0;
    for ( i = 0; i < file_Length; i++){
        sprintf(str,"%lf %lf %lf %lf\n",x[i],y[i],alt[i],yaw[i]);
        fputs(str,out);
    }

    fclose(conf);
    fclose(out);
}