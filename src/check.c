#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

double x[1000] = {0};
double y[1000] = {0};
double alt[1000] = {0};
double yaw[1000] = {0};
int file_Length = 0;


void read_config(char* config){
    // open configuration file
    FILE* conf = fopen(config,"r");
    // holder string
    char str[1000];
    double start[2];
    
    // read in the quadrotor initial position
    fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
    while(fscanf(conf,"%lf %lf %lf %lf",&x[file_Length],&y[file_Length],&alt[file_Length],&yaw[file_Length]) != EOF)
    {
      file_Length++;
    }

    fclose(conf);
}

void main()
{
	int i = 0;
	char blah[] = "config.txt";
	read_config(blah);

}