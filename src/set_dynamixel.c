#define EXTERN extern
#include "../include/quadcopter_main.h"
#define halfClosed 80
#define closedAngle 56
#define openAngle 80

int local_test_Status(DynamStatus *status1 , DynamStatus *status2 ,double angle){
    if ((abs(status1->cur_angle - angle) < 1) && (abs(status2->cur_angle - angle) < 1)){
        printf("Success  %.1lf  %.1lf\n",status1->cur_angle,status2->cur_angle);
        return 1;
    }
    else{
        printf("Failure  %.1lf  %.1lf\n",status1->cur_angle,status2->cur_angle);
        return 0;
    }
/*
    printf("\n\t Status For Servo ID #%d\n\n",status->id);
    if(status->cmd_mode)   printf("Command Mode: \t WHEEL \n");
    else           printf("Command Mode: \t JOINT \n");
    printf(" COMMANDED Values:\n");
    if(!(status->cmd_mode))   printf(" Angle:       %.1lf\n",status->cmd_angle);
    printf(" Speed:       %.1lf\n",status->cmd_speed);
    printf(" Torque:      %.1lf\n\n",status->cmd_torque);

    printf(" CURRENT Values:\n");
    printf(" Angle:       %.1lf\n",status->cur_angle);
    printf(" Speed:       %.1lf\n",status->cur_speed);
    printf(" Load:        %.1lf\n",status->cur_load);
    printf(" Voltage:     %.1lf\n",status->cur_volt);
    printf(" Temperatue:  %.1lf\n\n",status->cur_temp);
*/

}

int local_test_Speed(DynamStatus *status1 , DynamStatus *status2){
    if ((status1->cur_speed == 0 ) && (status2->cur_speed == 0)){
        return 1;
    }
    else return 0;
}

void * set_dynamixel(void * var){ 
    int stateNum = 0;
    int gripperflag=0; //slightly closed. default for take off //0 Moving, 1 open, 2 closed, 3 unknown
    int perch_success = 0; //0 never done, 1 success, 2 failed
    while(1){      
        switch(stateNum){
            case 0:
                printf("State 0\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_flag = CMD;
                bus.servo[0].cmd_mode = JOINT;
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_flag = CMD;
                bus.servo[1].cmd_mode = JOINT;
                bus.servo[1].cmd_flag = MODE;                   
                pthread_mutex_unlock(&dynamixel_mutex);
                printf ( "Press Enter to Open Slightly . . ." );
                fflush ( stdout );
                getchar();
                stateNum = 1;
                break;
            case 1:
                printf("State 1\n");
                pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_angle = 80; 
                        bus.servo[0].cmd_speed = 0.4; 
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[1].cmd_angle = 80; 
                        bus.servo[1].cmd_speed = 0.4; 
                        bus.servo[1].cmd_flag = CMD;
                        pthread_mutex_unlock(&dynamixel_mutex);
                printf ( "Press Enter to Open Full . . ." );
                fflush ( stdout );
                getchar();
                stateNum = 2;
                break;
            case 2:
             pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_angle = 150; 
                        bus.servo[0].cmd_speed = 0.4; 
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[1].cmd_angle = 150; 
                        bus.servo[1].cmd_speed = 0.4; 
                        bus.servo[1].cmd_flag = CMD;
                pthread_mutex_unlock(&dynamixel_mutex);
                printf ( "Press Enter to Close . . ." );
                fflush ( stdout );
                getchar();
                stateNum = 3;
                break;
            case 3:
            printf("State 0\n");
                pthread_mutex_lock(&dynamixel_mutex);
                bus.servo[0].cmd_flag = CMD;
                bus.servo[0].cmd_mode = WHEEL;
                bus.servo[0].cmd_flag = MODE;
                bus.servo[1].cmd_flag = CMD;
                bus.servo[1].cmd_mode = WHEEL;
                bus.servo[1].cmd_flag = MODE;                   
                pthread_mutex_unlock(&dynamixel_mutex);
                printf ( "Press Enter to Grip . . ." );
                fflush ( stdout );
                getchar();
                stateNum = 4;
                break;
            case 4:
                printf("State 1\n");
                pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_speed = -1; 
                        bus.servo[0].cmd_torque = 1;
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[1].cmd_torque = 1;
                        bus.servo[1].cmd_speed = -1; 
                        bus.servo[1].cmd_flag = CMD;
                        pthread_mutex_unlock(&dynamixel_mutex);
                printf ( "Press Enter to Release . . ." );
                fflush ( stdout );
                getchar();
                stateNum = 4;
                break;
            default:
                exit(1);    
        }
        /*
            case 0:
                printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);
                if (perch_success == 2)
                {
                    printf("Switch to Wheel Mode \n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_flag = CMD;
                    bus.servo[0].cmd_mode = WHEEL;
                    bus.servo[0].cmd_flag = MODE;
                    bus.servo[1].cmd_flag = CMD;
                    bus.servo[1].cmd_mode = WHEEL;
                    bus.servo[1].cmd_flag = MODE;                   
                    pthread_mutex_unlock(&dynamixel_mutex);
                    gripperflag = 0;
                    stateNum=2;
                }
                else if(gripperflag!=1)
                {
                        printf("Switch to Joint Mode \n");
                        stateNum=1;// next set point is intermediate waypoint
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[0].cmd_mode = JOINT; 
                        bus.servo[0].cmd_flag = MODE;
                        bus.servo[1].cmd_flag = CMD;
                        bus.servo[1].cmd_mode = JOINT; 
                        bus.servo[1].cmd_flag = MODE;
                        pthread_mutex_unlock(&dynamixel_mutex);
                        gripperflag = 0;   
                }
                else if (gripperflag == 1){
                        printf("Switch to Wheel Mode \n");
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[0].cmd_mode = WHEEL; 
                        bus.servo[0].cmd_flag = MODE;
                        bus.servo[1].cmd_flag = CMD;
                        bus.servo[1].cmd_mode = WHEEL; 
                        bus.servo[1].cmd_flag = MODE;                
                        pthread_mutex_unlock(&dynamixel_mutex);
                        gripperflag = 0;
                        stateNum=2;
                }       
                break;
            case 1:
                printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);
                if (gripperflag == 0){
                    printf("Move to Open\n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_angle = openAngle; 
                    bus.servo[0].cmd_speed = 0.4; 
                    bus.servo[0].cmd_flag = CMD;
                    bus.servo[1].cmd_angle = openAngle; 
                    bus.servo[1].cmd_speed = 0.4; 
                    bus.servo[1].cmd_flag = CMD;
                    pthread_mutex_unlock(&dynamixel_mutex);
                    gripperflag = 3;
                }
                else if (gripperflag == 3)
                {
                    printf("Check Status \n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_flag = STATUS;
                    bus.servo[1].cmd_flag = STATUS;
                    pthread_mutex_unlock(&dynamixel_mutex);
                    sleep(2);
                }
                if(local_test_Status(&(bus.servo[0]),&(bus.servo[1]),openAngle))//check if open completely
                {
                    if(perch_success > 0) //after a grip
                    {
                        stateNum=4;
                    }
                    else
                    { //before attempting perch operation
                        stateNum=0;
                        gripperflag=1;
                    }
                }

                break;
            case 2:
                printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);
                if (gripperflag ==3){
                    printf("Check Status\n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_flag = STATUS;
                    bus.servo[1].cmd_flag = STATUS;
                    pthread_mutex_unlock(&dynamixel_mutex);
                    sleep(2);
                }
                if (gripperflag == 0){
                    printf("Wheel Close\n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_torque = 1; 
                    bus.servo[0].cmd_speed = -1; 
                    bus.servo[0].cmd_flag = CMD;
                    bus.servo[1].cmd_torque = 1; 
                    bus.servo[1].cmd_speed = -1; 
                    bus.servo[1].cmd_flag = CMD;
                    pthread_mutex_unlock(&dynamixel_mutex);
                    gripperflag = 3;
                }
                else if (local_test_Speed(&(bus.servo[0]),&(bus.servo[1]))){
                    if(local_test_Status(&(bus.servo[0]),&(bus.servo[1]),closedAngle))// check if successfully closed
                    {
                        stateNum=3;
                        perch_success=1;
                        //turn off quad
                        gripperflag=2;
                    }
                    else 
                    {
                        printf("Switch to Joint Mode \n");
                        stateNum=1; //open gripper wide
                        perch_success = 2;
                        gripperflag = 0;   
                        pthread_mutex_lock(&dynamixel_mutex);
                        bus.servo[0].cmd_flag = CMD;
                        bus.servo[0].cmd_mode = JOINT; 
                        bus.servo[0].cmd_flag = MODE;
                        bus.servo[1].cmd_flag = CMD;
                        bus.servo[1].cmd_mode = JOINT; 
                        bus.servo[1].cmd_flag = MODE;
                        pthread_mutex_unlock(&dynamixel_mutex);
                    }
                }
                break;
            case 3:
                printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);
                sleep(10);
                //wait for command to take off again
                if (true) {
                    printf("Switch to Joint Mode \n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_flag = CMD;
                    bus.servo[0].cmd_mode = JOINT; 
                    bus.servo[0].cmd_flag = MODE;
                    bus.servo[1].cmd_flag = CMD;
                    bus.servo[1].cmd_mode = JOINT; 
                    bus.servo[1].cmd_flag = MODE;
                    pthread_mutex_unlock(&dynamixel_mutex);
                    gripperflag = 0;   
                    stateNum = 1;
                }
                break;
            case 4:
                printf("State %d GripperFlag %d Perch_success %d \n", stateNum, gripperflag ,perch_success);
                if (gripperflag == 0){
                    stateNum = 5;
                }
                if(perch_success==2)
                {
                    stateNum=0;// try to land again
                }
                else if(perch_success==1)
                {
                    printf("Close Grip \n");
                    pthread_mutex_lock(&dynamixel_mutex);
                    bus.servo[0].cmd_angle = halfClosed;
                    bus.servo[0].cmd_speed = 0.4;
                    bus.servo[0].cmd_flag = CMD;
                    bus.servo[1].cmd_angle = halfClosed;
                    bus.servo[1].cmd_speed = 0.4;
                    bus.servo[1].cmd_flag = CMD;
                    pthread_mutex_unlock(&dynamixel_mutex);  
                    gripperflag = 0;
                }
                break;
            case 5:
                printf("Done\n");
                stateNum = 7;
                break;
            default:
                exit(1);
                */
        fflush(stdout);
        sleep(1);
    }
}